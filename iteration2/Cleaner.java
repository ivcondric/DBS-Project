import au.com.bytecode.opencsv.CSVReader;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class Cleaner {

    public static void main(String[] args) {
        Cleaner cleaner = new Cleaner();
        String[] topLayerCleaned = cleaner.cleanup();
        cleaner.write(topLayerCleaned);
    }

    /**
     * nutzt das saubere Array um einen neuen csv-file zu erstellen
     */
    private void write(String[] topLayerCleaned) {
        try {
            /**
             * initialisiere Writer
             */
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("cleanedElection.csv"), "Cp1252"));
            System.out.println(topLayerCleaned.length);
            writer.write("id; handle; text; is_retweet; original_author; time; in_reply_to_screen_name; retweet_count; favorite_count" + System.getProperty("line.separator"));
            /**
             * iteriere über die Elemente des Arrays
             * jedes Element ist eine Zeile
             */
            for(String line : topLayerCleaned) {
                System.out.println(line);
                writer.write(line + System.getProperty("line.separator") );
            }
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Auslagerung der Initalisierungen etc. aus der main-Methode
     */
    private String[] cleanup() {
        CSVReader reader;
        /**
         * aus der Betrachtung des files wissen wir,
         * dass wir 6126 Zeilen mit Daten haben
         */
        String[] cleaned = new String[6126];
        try {
            /**
             * initiiere reader
             * in unserem Fall nutzen wir einen CSVReader
             * der frei verfügbaren opencsv-library,
             * was das Splitten der einzelnen Zeilen verinfacht,
             * da CSVReader auch Angaben wie ";;" korrekt liest
             */
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(ClassLoader.getSystemResourceAsStream("american-election-tweets.csv"), "Cp1252"));
            reader = new CSVReader(bufferedReader, ';', '\"', '\\');

            /**
             * überspringen der Bezeichner-Zeile
             */
            reader.readNext();


            makeCleaned(reader, cleaned);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return cleaned;
    }


    /**
     * Iteriert über jede einzelne Zeile des CSV-files und
     * beurteilt, ob die relevanten Spalteneinträge korrekt sind.
     * Beinhaltet außerdem einen counter id, der zu jeder Tweet-
     * Zeile hinzgefügt wird, um einen besseren Primärschlüssel zu haben
     */
    private String[] makeCleaned(CSVReader reader, String[] cleaned) throws IOException {
        String[] tweet;
        int id = 0;

        while ((tweet = reader.readNext()) != null) {
            id++;
            for(int i = 0; i < tweet.length; i++) {
                if (tweet[i] == null){
                    tweet[i] = "";
                }
            }

            /**
             * überprüft, ob das Handle tatsächlich einer der
             * Präsidentschaftskandidaten ist
             */

            boolean validPolitician = false;
            boolean politicianBool = true;
            if(tweet[0].equals("realDonaldTrump")){
                politicianBool  = true;
                validPolitician = true;

            } else if (tweet[0].equals("HillaryClinton")){
                politicianBool  = false;
                validPolitician = true;
            }


            /**
             * entfernt Zeilenumbrüche aus den Texten
             */
            tweet[1] = splitFor(tweet[1], "\n");


            /**
             * überprüft, ob is_retweet einen Boolean darstellt
             */
            boolean isRetweet = true;
            boolean validRetweet = false;
            if(tweet[2].equals("True")){
                isRetweet  = true;
                validRetweet = true;

            } else if (tweet[2].equals("False")){
                isRetweet  = false;
                validRetweet = true;
            }

            /**
             * überprüft, ob time einen tatsächlichen
             * timestamp darstellt
             * Da alle Timestamps in der Form "yyyy-MM-ddT HH:mm:ss",
             * also mit einem T, das nicht geparset werden kann, gegeben sind,
             * splittet diese Methode den Timestamp nach dem T auf und fügt ihn ohne
             * das T wieder zusammen.
             */
            tweet[4] = splitFor(tweet[4], "T");
            boolean validTP = isValidTimestamp(tweet[4]);

            /**
             * überprüft, ob retweet_Count und favorite_Count
             * Integer darstellen
             */
            boolean validRTCount = true;
            boolean validFVTCount = true;
            int retweetCount = 0;
            int favoriteCount = 0;
            try {
                retweetCount = Integer.parseInt(tweet[7]);
            } catch (NumberFormatException e){
                validRTCount = false;
            }

            try {
                favoriteCount = Integer.parseInt(tweet[8]);
            } catch (NumberFormatException e){
                validFVTCount = false;
            }


            /**
             * nur wenn bei den vorherigen Tests immer true
             * herausgekommen ist, wird die modifizierte Zeile
             * in das sauber Array aufgenommen
             */
            if(validPolitician && validRetweet && validTP && validRTCount && validFVTCount) {
                cleaned[id - 1] = id + ";" + politicianBool + ";\"" + tweet[1] + "\";" + isRetweet + ";" + tweet[3] + ";" + tweet[4] + ";" + tweet[5] + ";" + retweetCount + ";" + favoriteCount;
            }
        }
        return cleaned;
    }

    /**
     * versucht den angegebenen String mit dem gewünschten Format
     * zu parsen. Bei Erfolg wird true, bei Fehler false zurückgegeben
     */
    public boolean isValidTimestamp(String timeString) {
        SimpleDateFormat format = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            format.parse(timeString);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    /**
     * Diese Methode splittet den gegebenen String
     * nach dem "splitfor" auf und fügt ihn ohne
     * das "splitfor" wieder zusammen.
     */
    private String splitFor(String timeString, String splitfor) {
        String[] split = timeString.split(splitfor);
        split[0] = split[0] + " ";
        String together  = "";
        for(int i = 0; i < split.length; i++){
            together = together + split[i];
        }
        return together;
    }

}