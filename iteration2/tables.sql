CREATE TABLE public."Hashtag"
(
  text text NOT NULL,
  "amountTweets" integer,
  CONSTRAINT "PrimaryKeyHashtag" PRIMARY KEY (text)
)

CREATE TABLE public."Tweet"
(
  "ID" integer NOT NULL,
  "Handle" boolean,
  "Text" text,
  "isRetweet" boolean,
  "originalAuthor" text,
  "timestamp" timestamp without time zone,
  "inReplyTo" text,
  "retweetCount" integer,
  "favoriteCount" integer,
  CONSTRAINT "PrimaryKey" PRIMARY KEY ("ID")
)

CREATE TABLE public.has
(
  "ID" integer NOT NULL,
  text text NOT NULL,
  CONSTRAINT "PrimaryKeyHas" PRIMARY KEY ("ID", text),
  CONSTRAINT "Fremdschluessel" FOREIGN KEY ("ID")
      REFERENCES public."Tweet" ("ID") MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "Fremdschluessel2" FOREIGN KEY (text)
      REFERENCES public."Hashtag" (text) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)


CREATE TABLE public."tweetedWith"
(
  "Hashtag1" text NOT NULL,
  "Hashtag2" text NOT NULL,
  CONSTRAINT "PrimaryKeyTweetetWith" PRIMARY KEY ("Hashtag1", "Hashtag2"),
  CONSTRAINT "FremdschluesselTweetedWith1" FOREIGN KEY ("Hashtag1")
      REFERENCES public."Hashtag" (text) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT "FremdschluessselTweetedWith2" FOREIGN KEY ("Hashtag2")
      REFERENCES public."Hashtag" (text) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)