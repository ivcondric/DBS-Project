
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;

public class kMeansAlgo {

    private ArrayList<CoordinateTripel> coordinateTripels;
    private int k;

    public kMeansAlgo(ArrayList<CoordinateTripel> coordinateTripels, int k) {
        this.coordinateTripels = coordinateTripels;
        this.k = k;
    }

    /**
     * Methode zum Erstellen des neuen CSV-files
     */
    public void write(ArrayList<CoordinateTripel>[] clusterArray) {
        try {
            /**
             * initialisiere Writer
             */
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("clusters.csv"), "Cp1252"));
            System.out.println(clusterArray.length);
            writer.write("text; favoriteCoordinate; retweetCoordinate; clusterID" + System.getProperty("line.separator"));
            /**
             * iteriere über die Listen des Arrays
             * und innerhalb dieser Iteration über die Elemente
             * der Listen.
             */
            int i = 0;
            for(ArrayList<CoordinateTripel> cluster : clusterArray) {
                for (CoordinateTripel coordinateTripel : cluster) {
                    String oneEntry = coordinateTripel.getTextCoordinate() + "; "+ (int)(coordinateTripel.getFavoriteCoordinate()) + "; "+ (int)(coordinateTripel.getRetweetCoordinate()) + "; "+ i;
                    writer.write(oneEntry + System.getProperty("line.separator") );
                }
                i++;
            }
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * tatsächlicher k-Means-Algorithmus
     */
    public ArrayList<CoordinateTripel>[] algorithm() {
        /**
         * initialisiere zwei Arrays der Länge k in denen die aktuellen
         * und die neuen Cluster gespeichert werden, sowie ein
         * Array von Listen, in dem die Cluster gespeichert werden
         */
        CoordinateTripel[] clusterCenters = new CoordinateTripel[k];
        CoordinateTripel[] newCenters = new CoordinateTripel[k];
        ArrayList<CoordinateTripel>[] clusterArray = new ArrayList[k];

        /**
         * wähle k Hastags zufällig als erste Clustercenter aus, um
         * leere Cluster zu vermeiden
         */
        for (int i = 0; i < clusterCenters.length; i++) {
            clusterCenters[i] = coordinateTripels.get((int)(Math.random()*coordinateTripels.size()));
        }

        /**
         * das Array von neuen Clustercentern wird mit 0 in allen
         * Koordinaten initialisiert
         */
        for (int i = 0; i < clusterCenters.length; i++) {
            newCenters[i] = new CoordinateTripel("", 0, 0);
        }

        /**
         * zähle wie viele Iterationen man schon hat und notiere,
         * ob man sich in der ersten Iterationen befindet
         */
        int iterations = 0;
        boolean firstIteration = true;
        int differences = 1000;
        /**
         * Schleifeninhalt wird so oft ausgeführt, bis es keine Unterschiede
         * in den Clustern mehr gibt
         */
        while (differences > 0){
            System.out.println(iterations);
            /**
             * erstelle ein neues Array von Listen, in dem die neuen Cluster
             * gespeichert werden, um sie mit den aktuellen Clustern vergleichen
             * zu können
             */
            ArrayList<CoordinateTripel>[] tempClusterArray = new ArrayList[k];
            for (int i = 0; i < tempClusterArray.length; i++) {
                tempClusterArray[i] = new ArrayList<CoordinateTripel>();
            }

            /**
             * füge die Tripel nacheinander in die Liste hinzu, dessen
             * Index im clusterCenter-Array auf das nächste Center zeigt
             */
            for (CoordinateTripel tripel : coordinateTripels) {
                tempClusterArray[getClosestIndex(tripel, clusterCenters)].add(tripel);
            }

            /***
             * falls man sich nicht in der ersten Iteration befindet, werden
             * die aktuellen Clustercenter durch die neuen Center abgelöst
             * und man zählt die Unterschiede der aktuellen mit den neuen
             * Clustern
             */
            if(!firstIteration) {
                clusterCenters = newCenters;
                differences = getDifferences(tempClusterArray, clusterArray);

            } else {
                firstIteration = false;
            }
            /**
             * löse die aktuellen durch die neuen Ckuster ab und mache aus
             * ihnen neue Clustercenter
             */
            clusterArray = tempClusterArray;
            newCenters = makeClusterCenters(tempClusterArray);
            iterations++;
        }

        /**
         * Abschließend werden die letzten Clustercenter an ihre jeweiligen
         * Listen angehängt
         */
        int j = 0;
        for (ArrayList<CoordinateTripel> tripelArrayList : clusterArray) {
            tripelArrayList.add(clusterCenters[j]);
            j++;
        }

        return clusterArray;
    }

    public CoordinateTripel[] makeClusterCenters(ArrayList<CoordinateTripel>[] clusterArray) {
        CoordinateTripel[] centers = new CoordinateTripel[k];
        int j = 0;
        /**
         * Summiere die Retweets und Favorites der Tripel eines Clusters
         * und teile diese Summen durch die Anzahl der Tripel
         */
        for (ArrayList<CoordinateTripel> cluster : clusterArray) {
            int i = 0;
            CoordinateTripel sum = new CoordinateTripel("", 0, 0);
            for (CoordinateTripel coordinateTripel : cluster) {
                sum = coordinateAddition(sum, coordinateTripel);
                i++;
            }
            /**
             * Sonderfall: falls Cluster leer ist, werden die
             * Koordinaten des Centers auf 0 gesetzt
             */
            if(i == 0){
                centers[j] = new CoordinateTripel("", 0, 0);
            } else {
                centers[j] = coordinateDivision(sum, i);
            }
            j++;
        }
        return centers;
    }

    private int getDifferences(ArrayList<CoordinateTripel>[] tripelArray1, ArrayList<CoordinateTripel>[] tripelArray2) {
        int differences = 0;
        /**
         * Falls die größe zweier Listen am selben Index in den verschiedenen Arrays unterschiedliche
         * Länge haben, gibt es schonmal so viele Unterschiede, wie die Differenz beträgt.
         * Enthält dann noch die kleinere Liste Elemente, die die größere nicht enthält,
         * gibt es pro solchem Element einen Unterschied mehr
         */
        for (int i = 0; i < tripelArray1.length; i++){
            if(tripelArray1[i].size() <= tripelArray2[i].size()) {
                differences = differences + (tripelArray2[i].size() - tripelArray1[i].size());
                for (int j = 0; j < tripelArray1[i].size(); j++){
                    if(!tripelArray2[i].contains(tripelArray1[i].get(j))){
                        differences++;
                    }
                }
            } else {
                differences = differences + (tripelArray1[i].size() - tripelArray2[i].size());
                for (int j = 0; j < tripelArray2[i].size(); j++){
                    if(!tripelArray1[i].contains(tripelArray2[i].get(j))){
                        differences++;
                    }
                }

            }
        }
        return differences;
    }

    private int getClosestIndex(CoordinateTripel tripel, CoordinateTripel[] clusterCenters) {
        double minimumDistance = getDistance(tripel, clusterCenters[0]);
        int i = 0;
        int k = 0;
        /**
         * iteriere über das Array der Center und merke dabei den Index
         * des Centers mit dem kleinsten Unterschied zum gegebenen Punkt
         */
        for (CoordinateTripel clusterCenter : clusterCenters) {
            if (getDistance(tripel, clusterCenter) < minimumDistance) {
                minimumDistance = getDistance(tripel, clusterCenter);
                k = i;
            }
            i++;
        }
        return k;
    }


    private double getDistance(CoordinateTripel tripel1, CoordinateTripel tripel2) {
        /**
         * Der Unterschied ist die Addition aus der Differenz der Retweets und der Differenz
         * der Favorits
         */
        double rtSub = Math.abs(tripel1.getRetweetCoordinate() - tripel2.getRetweetCoordinate());
        double fvtSub = Math.abs(tripel1.getFavoriteCoordinate() - tripel2.getFavoriteCoordinate());
        return rtSub + fvtSub;
    }



    /**
     * Hilfsmethode, die die einzelnen Koordinaten eines Tripels
     * durch eine Ganzzahl teilt
     */
    private CoordinateTripel coordinateDivision(CoordinateTripel sum, int i) {
        double rtDivided = sum.getRetweetCoordinate() / i;
        double fvtDivided = sum.getFavoriteCoordinate() / i;
        return new CoordinateTripel("", rtDivided, fvtDivided);
    }

    /**
     * Hilfsmethode, die die Koordinaten zweier Tripel aufaddiert
     */
    private CoordinateTripel coordinateAddition(CoordinateTripel coordinateTripelA, CoordinateTripel coordinateTripelB) {
        double rtAdded = coordinateTripelA.getRetweetCoordinate() + coordinateTripelB.getRetweetCoordinate();
        double fvtAdded = coordinateTripelA.getFavoriteCoordinate() + coordinateTripelB.getFavoriteCoordinate();
        return new CoordinateTripel("", rtAdded, fvtAdded);
    }
}