
public class CoordinateTripel {

    private String textCoordinate;
    private double retweetCoordinate;
    private double favoriteCoordinate;

    /**
     * Diese Klasse dient nur zur Speicherung der im Konstruktor
     * angegebenen Koordinaten.
     * @param textCoordinate gibt den Hashtag-Text wieder
     * @param retweetCoordinate gibt die aufaddierten Retweets wieder
     * @param favoriteCoordinate gibt die aufaddierten Favorits wieder
     */
    public CoordinateTripel(String textCoordinate, double retweetCoordinate, double favoriteCoordinate){
        this.textCoordinate = textCoordinate;
        this.retweetCoordinate = retweetCoordinate;
        this.favoriteCoordinate = favoriteCoordinate;
    }

    /**
     * Getter und Setter
     */
    public String getTextCoordinate() {
        return textCoordinate;
    }

    public void setTextCoordinate(String textCoordinate) {
        this.textCoordinate = textCoordinate;
    }

    public double getRetweetCoordinate() {
        return retweetCoordinate;
    }

    public void setRetweetCoordinate(double retweetCoordinate) {
        this.retweetCoordinate = retweetCoordinate;
    }

    public double getFavoriteCoordinate() {
        return favoriteCoordinate;
    }

    public void setFavoriteCoordinate(double favoriteCoordinate) {
        this.favoriteCoordinate = favoriteCoordinate;
    }
}
