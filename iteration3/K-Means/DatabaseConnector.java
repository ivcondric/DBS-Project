
import java.sql.*;
import java.util.ArrayList;

public class DatabaseConnector {

    public ArrayList<CoordinateTripel> makeCoordinates() {
        ArrayList<CoordinateTripel> tripels = new ArrayList<CoordinateTripel>();
        Connection connect = null;
        Statement statet = null;
        try {
            /**
             * stelle eine Verbindung zur postgre-Datenbank her
             */
            Class.forName("org.postgresql.Driver");
            connect = DriverManager.getConnection("jdbc:postgresql://agdbs-edu01.imp.fu-berlin.de/BernieIsMyLover", "student", "password");
            statet = connect.createStatement();
            /**
             * SQL-Anfrage, die die Summe der Retweets und Favorits eines Joins der has-Tabelle mit Tweet wiedergibt,
             * nach den hashtag-texten gruppiert
             */
            ResultSet result = statet.executeQuery("SELECT SUM(T.\"retweetCount\") AS rtcount, SUM(T.\"favoriteCount\") AS fvtcount, H.hashtag " +
                    "                               FROM \"Tweet\" T JOIN has H ON H.\"ID\" = T.\"ID\" " +
                    "                               GROUP BY hashtag");
            /**
             * Füge einen hashtag nach dem anderen in Form eines CoordinateTripels in eine Liste ein
             */
            while ( result.next() ) {
                String  text = result.getString("hashtag");
                int retweets = result.getInt("rtcount");
                int favorites = result.getInt("fvtcount");
                tripels.add(new CoordinateTripel(text, retweets, favorites));
            }
            /**
             * schließe Verbindung
             */
            result.close();
            statet.close();
            connect.close();
        } catch(Exception e){
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
            System.exit(0);
        }

        /**
         * gib Liste zurück
         */
        return tripels;
    }
}
