
import java.util.ArrayList;

public class kMeansMain {

    public static void  main(String[] args){
        /**
         * initialisiere k, die Verbidung zur Datenbank, sowie den kMeans-Algorithmus
         */
        int k = 5;
        DatabaseConnector connector = new DatabaseConnector();
        ArrayList<CoordinateTripel> finishedCoordinates = connector.makeCoordinates();

        kMeansAlgo algo = new kMeansAlgo(finishedCoordinates, k);
        ArrayList<CoordinateTripel>[] finishedClusters = algo.algorithm();

        /**
         * Ausgabe der Cluster, das letzte Element eines Clusters ist das Center
         */
        int i = 1;
        for (ArrayList<CoordinateTripel> finishedCluster : finishedClusters) {
            CoordinateTripel clustercenter = finishedCluster.get(finishedCluster.size()-1);
            finishedCluster.remove(finishedCluster.size()-1);
            System.out.println("Cluster #" + i + ":");
            System.out.println(" Cluster-Center: RetweetKoordinate " + clustercenter.getRetweetCoordinate() +" FavoriteKoordinate " + clustercenter.getFavoriteCoordinate());
            for (CoordinateTripel tripel : finishedCluster) {
                System.out.println("  Hashtag: \"" + tripel.getTextCoordinate() + "\"; RetweetKoordinate: " + tripel.getRetweetCoordinate() +"; FavoriteKoordinate: " + tripel.getFavoriteCoordinate());
            }
            i++;
        }

        /**
         * Schreibe die fertige Cluster OHNE Center in eine CSV
         */
        algo.write(finishedClusters);
    }
}
